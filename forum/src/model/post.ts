import { User } from './user';

export interface Post {
  id: number | null;
  content: string;
  threadId: number;
  user: User;
}
