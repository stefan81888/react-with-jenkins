export interface Thread {
  id?: number;
  title: string;
  topicId: number;
}
