import { Topic } from './topic';

export interface Subforum {
  title: string;
  id?: number;
  topics: Topic[];
}
