export interface Topic {
  title: string;
  id?: number;
}
