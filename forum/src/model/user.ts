export interface User {
  id?: number;
  username: string;
  password: string;
  imageUrl: string;
  isAdmin: boolean;
  isLoggedIn: boolean;
}
