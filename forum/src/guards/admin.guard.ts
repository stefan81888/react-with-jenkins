import { LoginService } from '../services/login.service';

export const adminGuard = (to: any, from: any, next: any) => {
  const loginService = new LoginService();

  if (to.meta.admin) {
    if (loginService.isLoggedUserAdmin()) {
      next();
    }
    next.redirect('/home');
  } else {
    next();
  }
};
