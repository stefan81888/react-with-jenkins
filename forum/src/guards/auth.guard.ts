import { LoginService } from '../services/login.service';

export const authGuard = (to: any, from: any, next: any) => {
  const loginService = new LoginService();

  if (to.meta.auth) {
    if (loginService.isUserLoggedIn()) {
      next();
    }
    next.redirect('/');
  } else {
    next();
  }
};
