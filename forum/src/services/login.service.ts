import { User } from '../model/user';

export class LoginService {
  logUserIn(user: User): void {
    localStorage.setItem('username', user?.username);
    localStorage.setItem('imageUrl', user?.imageUrl);
    localStorage.setItem('password', user?.password);
    localStorage.setItem('isAdmin', user?.isAdmin.toString());
    localStorage.setItem('id', (user?.id as unknown) as string);
  }

  logUserOut(): void {
    localStorage.clear();
  }

  isUserLoggedIn(): boolean {
    return !!localStorage.getItem('username');
  }

  isLoggedUserAdmin(): boolean {
    return this.isUserLoggedIn() && this.getLoggedUser().isAdmin;
  }

  getLoggedUser(): User {
    if (this.isUserLoggedIn()) {
      return {
        username: localStorage.getItem('username') as string,
        imageUrl: localStorage.getItem('imageUrl') as string,
        password: localStorage.getItem('password') as string,
        isAdmin: localStorage.getItem('isAdmin') === 'true',
        id: parseInt(localStorage.getItem('id') as string, 10),
        isLoggedIn: true,
      };
    }

    return {} as User;
  }
}
