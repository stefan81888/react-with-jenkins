import { LoginService } from './login.service';

const loginService = new LoginService();

describe('Login service', () => {
  it('logs user in', () => {
    loginService.logUserIn({
      imageUrl: 'image',
      isAdmin: false,
      password: '***',
      username: 'username1',
      id: 0,
      isLoggedIn: true,
    });

    expect(localStorage.getItem('username') as string).toBe('username1');
    expect(localStorage.getItem('imageUrl') as string).toBe('image');
    expect(localStorage.getItem('isAdmin') as string).toBe('false');

    localStorage.clear();
  });

  it('checks if user is logged in', () => {
    loginService.logUserIn({
      imageUrl: 'image',
      isAdmin: false,
      password: '***',
      username: 'username1',
      id: 0,
      isLoggedIn: true,
    });

    expect(loginService.isUserLoggedIn()).toBe(true);

    localStorage.clear();
  });

  it('logs user out', () => {
    loginService.logUserIn({
      imageUrl: 'image',
      isAdmin: false,
      password: '***',
      username: 'username1',
      id: 0,
      isLoggedIn: true,
    });

    loginService.logUserOut();
    expect(loginService.isUserLoggedIn()).toBe(false);
  });

  it('checks if admin is logged in', () => {
    loginService.logUserIn({
      imageUrl: 'image',
      isAdmin: true,
      password: '***',
      username: 'username1',
      id: 0,
      isLoggedIn: true,
    });

    expect(loginService.isLoggedUserAdmin()).toBe(true);

    localStorage.clear();
  });

  it('gets logged user', () => {
    loginService.logUserIn({
      imageUrl: 'image',
      isAdmin: false,
      password: '***',
      username: 'username1',
      id: 0,
      isLoggedIn: true,
    });

    const user = loginService.getLoggedUser();
    expect(user.username).toBe('username1');
    expect(user.imageUrl).toBe('image');
    expect(user.isAdmin).toBe(false);

    localStorage.clear();
  });
});
