import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import { act } from 'react-dom/test-utils';
import { unmountComponentAtNode } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

const initialState = {};
const mockStore = configureStore();
const store = mockStore(initialState);

let container: any = null;
beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

describe('App', () => {
  it('renders content', async () => {
    await act(() => {
      render(
        <Provider store={store}>
          {' '}
          <App />{' '}
        </Provider>,
        container
      );
    });

    const content = document.querySelector('.content');
    expect(content).toBeTruthy();
  });

  it('renders header', async () => {
    await act(() => {
      render(
        <Provider store={store}>
          {' '}
          <App />{' '}
        </Provider>,
        container
      );
    });

    const header = document.querySelector('[data-testid=header]');
    expect(header).toBeTruthy();
  });
});
