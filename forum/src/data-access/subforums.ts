import axios from 'axios';
import { baseUrl } from './base-url';
import { Subforum } from '../model/subforum';

export class SubforumService {
  getSubforums = async (): Promise<Subforum[]> => {
    const response = await axios.get(`${baseUrl}subforums`);
    return response.data;
  };

  editSubforum = async (subforum: Subforum): Promise<Subforum> => {
    const response = await axios.put(`${baseUrl}subforums/${subforum.id}`, subforum);
    return response.data;
  };

  addSubforum = async (subforum: Subforum): Promise<Subforum> => {
    const response = await axios.post(`${baseUrl}subforums`, subforum);
    return response.data;
  };

  removeSubforum = async (subforum: Subforum): Promise<Subforum> => {
    const response = await axios.delete(`${baseUrl}subforums/${subforum.id}`);
    return response.data;
  };
}
