import axios from 'axios';
import { baseUrl } from './base-url';
import { User } from '../model/user';

export class UsersService {
  getUsers = async (): Promise<User[]> => {
    const response = await axios.get(`${baseUrl}users`);
    return response.data;
  };

  addUser = async (user: User): Promise<User> => {
    const response = await axios.post(`${baseUrl}users`, user);
    return response.data;
  };

  editUser = async (user: User): Promise<User> => {
    const response = await axios.put(`${baseUrl}users/${user.id}`, user);
    return response.data;
  };

  deleteUser = async (user: User): Promise<User> => {
    const response = await axios.delete(`${baseUrl}users/${user.id}`);
    return response.data;
  };
}
