import axios from 'axios';
import { baseUrl } from './base-url';
import { Topic } from '../model/topic';

export class TopicService {
  getTopicById = async (topicId: number): Promise<Topic> => {
    const response = await axios.get(`${baseUrl}topics/${topicId}`);
    return response.data;
  };

  addTopic = async (topic: Topic): Promise<Topic> => {
    const response = await axios.post(`${baseUrl}topics`, topic);
    return response.data;
  };

  removeTopic = async (topic: Topic): Promise<Topic> => {
    const response = await axios.delete(`${baseUrl}topics/${topic.id}`);
    return response.data;
  };
}
