import axios from 'axios';
import { baseUrl } from './base-url';
import { Post } from '../model/post';

export class PostsService {
  getPosts = async (): Promise<Post[]> => {
    const response = await axios.get(`${baseUrl}posts`);
    return response.data;
  };

  addPost = async (post: Post): Promise<void> => {
    await axios.post(`${baseUrl}posts`, post);
  };

  editPost = async (post: Post): Promise<void> => {
    await axios.put(`${baseUrl}posts/${post.id}`, post);
  };

  deletePost = async (post: Post): Promise<Post> => {
    const response = await axios.delete(`${baseUrl}posts/${post.id}`);
    return response.data;
  };
}
