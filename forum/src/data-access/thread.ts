import axios from 'axios';
import { baseUrl } from './base-url';
import { Thread } from '../model/thread';

export class ThreadService {
  getThreads = async (): Promise<Thread[]> => {
    const response = await axios.get(`${baseUrl}threads`);
    return response.data;
  };

  addThread = async (thread: Thread): Promise<Thread> => {
    const response = await axios.post(`${baseUrl}threads`, thread);
    return response.data;
  };

  removeThread = async (thread: Thread): Promise<Thread> => {
    const response = await axios.delete(`${baseUrl}threads/${thread.id}`);
    return response.data;
  };
}
