import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { unmountComponentAtNode } from 'react-dom';
import AddUser from './AddUser';

describe('Add user component', () => {
  let container: any = null;
  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it('adds user', async () => {
    const onAddUserCallback = jest.fn();

    await act(async () => {
      render(<AddUser buttonLabel={'add'} onAddUser={onAddUserCallback} />, container);
    });

    const usernameInput = document.querySelector('[data-testid=username]');
    fireEvent.change(usernameInput as Element, { target: { value: 'test' } });
    const passwordInput = document.querySelector('[data-testid=password]');
    fireEvent.change(passwordInput as Element, { target: { value: 'test' } });
    const imageInput = document.querySelector('[data-testid=image]');
    fireEvent.change(imageInput as Element, { target: { value: 'test' } });
    const addPostButton = document.querySelector('[data-testid=add]');

    act(() => {
      addPostButton?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });

    expect(onAddUserCallback).toHaveBeenCalled();
  });
});
