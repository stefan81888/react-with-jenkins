import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { User } from '../../model/user';
import './AddUser.css';

export default function AddUser(props: {
  onAddUser: CallableFunction;
  buttonLabel: string;
  user?: User;
}) {
  const [user, setUser] = useState<User>({ ...(props.user as User), isAdmin: false });

  const onUserFormChange = (event: any): void => {
    const { name: propertyName, value } = event.target;
    setUser({ ...user, [propertyName]: value });
  };

  const isUserFormDisabled = (): boolean => {
    return user && (!user.username || !user.password || !user.imageUrl);
  };

  return (
    <div className="add-user-wrapper">
      <form className="flex flex-column flex-center">
        <TextField
          inputProps={{ 'data-testid': 'username' }}
          placeholder="Username"
          type="text"
          name="username"
          value={user?.username || ''}
          onChange={onUserFormChange}
        />
        <TextField
          inputProps={{ 'data-testid': 'password' }}
          className="margin-top-10"
          placeholder="Password"
          type="text"
          name="password"
          value={user?.password || ''}
          onChange={onUserFormChange}
        />
        <TextField
          inputProps={{ 'data-testid': 'image' }}
          className="margin-top-10 margin-bottom-10"
          placeholder="Profile image url"
          type="text"
          name="imageUrl"
          value={user?.imageUrl || ''}
          onChange={onUserFormChange}
        />
        <Button
          data-testid={props.buttonLabel}
          disabled={isUserFormDisabled()}
          className="margin-top-10 margin-left-10 add-user-button"
          onClick={() => {
            props.onAddUser(user);
            setUser({
              username: '',
              password: '',
              imageUrl: '',
              isAdmin: false,
              isLoggedIn: false,
            });
          }}
          variant="contained"
          color="primary"
        >
          {props.buttonLabel}
        </Button>
      </form>
    </div>
  );
}
