import React from 'react';
import { render } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { unmountComponentAtNode } from 'react-dom';
import Post from './Post';
import { Post as PostModel } from '../../model/post';
import { User } from '../../model/user';

const postMock = {
  id: 0,
  content: 'Test content',
  user: { username: 'test-username', isAdmin: false, imageUrl: 'test-url' },
} as PostModel;

const onPostEditCallback = jest.fn();
const onPostDeleteCallback = jest.fn();

describe('Post component', () => {
  let container: any = null;
  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it('displays user username', async () => {
    await act(async () => {
      render(
        <Post
          user={{} as User}
          post={postMock}
          onPostEdit={onPostEditCallback}
          onPostDelete={onPostDeleteCallback}
        />,
        container
      );
    });

    const username = document.querySelector('[data-testid=test-username]');

    expect(username).toBeTruthy();
    expect(username?.textContent).toBe('test-username');
  });

  it('displays user avatar', async () => {
    await act(async () => {
      render(
        <Post
          user={{} as User}
          post={postMock}
          onPostEdit={onPostEditCallback}
          onPostDelete={onPostDeleteCallback}
        />,
        container
      );
    });

    const avatar = document.querySelector('[data-testid=test-url]');

    expect(avatar).toBeTruthy();
    expect(avatar?.getAttribute('src')).toBe('test-url');
  });

  it('displays post content', async () => {
    await act(async () => {
      render(
        <Post
          user={{} as User}
          post={postMock}
          onPostEdit={onPostEditCallback}
          onPostDelete={onPostDeleteCallback}
        />,
        container
      );
    });

    const postContent = document.querySelector('[data-testid=content]');

    expect(postContent).toBeTruthy();
    expect(postContent?.textContent).toBe('Test content');
  });

  it('does not allow post to be edited when user is not logged in', async () => {
    await act(async () => {
      render(
        <Post
          user={{} as User}
          post={postMock}
          onPostEdit={onPostEditCallback}
          onPostDelete={onPostDeleteCallback}
        />,
        container
      );
    });

    const editPostLink = document.querySelector('[data-testid=post-0]');

    expect(editPostLink).toBeFalsy();
  });

  it('edits post when post author is logged in', async () => {
    await act(async () => {
      render(
        <Post
          user={{ username: 'test-username', isAdmin: false } as User}
          post={postMock}
          onPostEdit={onPostEditCallback}
          onPostDelete={onPostDeleteCallback}
        />,
        container
      );
    });

    const editPostLink = document.querySelector('[data-testid=post-0]');
    expect(editPostLink).toBeTruthy();

    act(() => {
      editPostLink?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });

    expect(onPostEditCallback).toHaveBeenCalledWith(postMock);
  });

  it('deletes post when admin user is logged in', async () => {
    await act(async () => {
      render(
        <Post
          user={{ username: 'test-username', isAdmin: true } as User}
          post={postMock}
          onPostEdit={onPostEditCallback}
          onPostDelete={onPostDeleteCallback}
        />,
        container
      );
    });

    const deletePost = document.querySelector('[data-testid="post-delete-Test content"]');
    expect(deletePost).toBeTruthy();

    act(() => {
      deletePost?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });

    const confirmButton = document.querySelector('[data-testid=confirm]');

    act(() => {
      confirmButton?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });

    expect(onPostDeleteCallback).toHaveBeenCalledWith(postMock);
  });
});
