import React, { useState } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { Post as PostModel } from '../../model/post';
import './Post.css';
import ConfirmationModal from '../confirmation-modal/ConfirmationModal';
import { User } from '../../model/user';

export default function Post(props: {
  post: PostModel;
  onPostEdit: CallableFunction;
  onPostDelete: CallableFunction;
  user: User;
}) {
  const post = props.post;
  const user = props.user;
  const [isConfirmationModalOpen, setIsConfirmationModalOpen] = useState<boolean>(false);

  const onDeletionConfirmed = (confirmed: true) => {
    if (confirmed) {
      props.onPostDelete(post);
    }

    setIsConfirmationModalOpen(false);
  };

  return (
    <div>
      <Card>
        <CardContent>
          <div className="flex">
            <div className="user-info flex flex-column">
              <div data-testid={post.user.username}>{post.user.username}</div>
              <img
                data-testid={post.user.imageUrl}
                className="margin-top-10 avatar author"
                src={post.user.imageUrl}
                alt=""
              />
            </div>
            <div className="post-content flex flex-column max-width">
              <p id={post.content} data-testid="content" className="float-left post-text">
                {post.content}
              </p>
              <div className="margin-bottom-10 margin-h-10">
                <hr className="margin-top-10 margin-bottom-10 separator" />
                <div className="flex options-container">
                  {post.user.username === user.username ? (
                    <span
                      data-testid={'post-' + post.id}
                      className="edit-button cursor-pointer"
                      onClick={() => {
                        props.onPostEdit(post);
                      }}
                    >
                      Edit
                    </span>
                  ) : null}
                  {user.isAdmin ? (
                    <span
                      data-testid={'post-delete-' + post.content}
                      className="cursor-pointer delete-post"
                      onClick={() => {
                        setIsConfirmationModalOpen(true);
                      }}
                    >
                      Delete
                    </span>
                  ) : null}
                </div>
              </div>
            </div>
          </div>
        </CardContent>
      </Card>
      <ConfirmationModal
        onConfirmationModalClose={(confirmed: any) => onDeletionConfirmed(confirmed)}
        open={isConfirmationModalOpen}
        message="Are you sure you want to delete this post?"
      />
    </div>
  );
}
