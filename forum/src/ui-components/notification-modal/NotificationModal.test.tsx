import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { unmountComponentAtNode } from 'react-dom';
import NotificationModal from './NotificationModal';

describe('Notification modal component', () => {
  let container: any = null;
  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it('dispalys message in modal', async () => {
    await act(async () => {
      render(
        <NotificationModal
          message="Hello World!"
          onNotificationModalClose={jest.fn()}
          open={true}
        />,
        container
      );
    });

    const message = document.querySelector('[data-testid=message]');

    expect(message?.textContent).toBe('Hello World!');
  });
});
