import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';

export default function NotificationModal(props: {
  message: string;
  open: boolean;
  onNotificationModalClose: any;
}) {
  return (
    <Dialog open={props.open} onClose={props.onNotificationModalClose}>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          <span data-testid="message">{props.message}</span>
        </DialogContentText>
      </DialogContent>
    </Dialog>
  );
}
