import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { unmountComponentAtNode } from 'react-dom';
import ManageUsers from './ManageUsers';

const onAddAdminCallback = jest.fn();
const onEditUserCallback = jest.fn();
const onDeleteUserCallback = jest.fn();

describe('Manage users component', () => {
  let container: any = null;
  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it('displays add admin component', async () => {
    await act(async () => {
      render(
        <ManageUsers
          users={[]}
          addAdmin={onAddAdminCallback}
          editUser={onEditUserCallback}
          deleteUser={onDeleteUserCallback}
        />,
        container
      );
    });

    const addUserComponent = document.querySelector('[data-testid=add-user]');

    expect(addUserComponent).toBeTruthy();
  });

  it('displays dropdowns for managing users', async () => {
    await act(async () => {
      render(
        <ManageUsers
          users={[]}
          addAdmin={onAddAdminCallback}
          editUser={onEditUserCallback}
          deleteUser={onDeleteUserCallback}
        />,
        container
      );
    });

    const addAdmin = document.querySelector('[data-testid=add-admin]');
    const removeAdmin = document.querySelector('[data-testid=remove-admin]');
    const deleteUser = document.querySelector('[data-testid=delete-user]');

    expect(addAdmin).toBeTruthy();
    expect(removeAdmin).toBeTruthy();
    expect(deleteUser).toBeTruthy();
  });
});
