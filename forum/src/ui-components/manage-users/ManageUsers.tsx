import React, { useState, useEffect } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Divider from '@material-ui/core/Divider';
import NotificationModal from '../notification-modal/NotificationModal';
import { User } from '../../model/user';
import AddUser from '../add-user/AddUser';
import './ManageUsers.css';
import UsersDropdown from './ui-components/users-dropdown/UsersDropdown';

export default function ManageUsers(props: {
  addAdmin: CallableFunction;
  editUser: CallableFunction;
  deleteUser: CallableFunction;
  users: User[];
}) {
  const [isNotificationModalOpen, setIsNotificationModalOpen] = useState<boolean>(false);
  const [users, setUsers] = useState<User[]>(props.users);
  const [regularUsers, setRegularUsers] = useState<User[]>(props.users.filter((x) => !x.isAdmin));
  const [adminUsers, setAdminUsers] = useState<User[]>(props.users.filter((x) => x.isAdmin));

  useEffect(() => {
    setUsers(props.users);
  }, [props.users]);

  useEffect(() => {
    setRegularUsers(users.filter((x) => !x.isAdmin));
  }, [users]);

  useEffect(() => {
    setAdminUsers(users.filter((x) => x.isAdmin));
  }, [users]);

  const addAdminUser = (user: User): void => {
    const userFromDb = users.find((u) => u.username === user.username);
    if (userFromDb) {
      setIsNotificationModalOpen(true);
    } else {
      props.addAdmin({ ...user, isAdmin: true });
    }
  };

  const onNotificationModalClose = (): void => {
    setIsNotificationModalOpen(false);
  };

  const addAdminRightsToUser = (selectedUserId: number): void => {
    const newAdmin = users.find((x) => x.id === selectedUserId);
    if (newAdmin) {
      newAdmin.isAdmin = true;
      props.editUser(newAdmin);
    }
  };

  const removeAdminRightsOfUser = (selectedUserId: number): void => {
    const formerAdmin = users.find((x) => x.id === selectedUserId);
    if (formerAdmin) {
      formerAdmin.isAdmin = false;
      props.editUser(formerAdmin);
    }
  };

  const deleteUser = (selectedUserId: number): void => {
    const userToBeDeleted = users.find((x) => x.id === selectedUserId);
    if (userToBeDeleted) {
      props.deleteUser(userToBeDeleted);
    }
  };

  return (
    <div>
      <Card>
        <CardContent className="flex flex-column">
          <h2>Manage users</h2>
          <Divider className="margin-top-10 margin-bottom-10" />
          <div className="flex flex-column">
            <div data-testid="add-user">
              <AddUser
                buttonLabel="Add admin user"
                onAddUser={(user: User) => addAdminUser(user)}
              />
            </div>
            <Divider />
            <div className="flex">
              <div data-testid="add-admin">
                <UsersDropdown
                  onSettingAdmin={(selectedUserId: number) => addAdminRightsToUser(selectedUserId)}
                  users={regularUsers}
                  buttonLabel="Add admin"
                />
              </div>
              <div data-testid="remove-admin">
                <UsersDropdown
                  onSettingAdmin={(selectedUserId: number) =>
                    removeAdminRightsOfUser(selectedUserId)
                  }
                  users={adminUsers}
                  buttonLabel="Remove admin"
                />
              </div>
              <div data-testid="delete-user">
                <UsersDropdown
                  onSettingAdmin={(selectedUserId: number) => deleteUser(selectedUserId)}
                  users={users}
                  buttonLabel="Delete user"
                />
              </div>
            </div>
          </div>
        </CardContent>
      </Card>
      <NotificationModal
        onNotificationModalClose={onNotificationModalClose}
        open={isNotificationModalOpen}
        message="Username already exists"
      />
    </div>
  );
}
