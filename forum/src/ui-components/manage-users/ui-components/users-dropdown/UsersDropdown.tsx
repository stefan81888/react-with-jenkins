import React, { useState, useEffect } from 'react';
import { User } from '../../../../model/user';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import './UsersDropdown.css';

export default function UsersDropdown(props: {
  users: User[];
  onSettingAdmin: CallableFunction;
  buttonLabel: string;
}) {
  const [users, setUsers] = useState<User[]>(props.users);
  const [selectedUserId, setSelectedUserId] = useState<number | string>('');

  useEffect(() => {
    setUsers(props.users);
  }, [props.users]);

  const addAdmin = (): void => {
    props.onSettingAdmin(selectedUserId);
    setSelectedUserId('');
  };

  return (
    <div className="flex flex-column new-admin">
      <FormControl>
        <InputLabel id="placeholder">Select user</InputLabel>
        <Select
          data-testid={'select-' + props.buttonLabel}
          labelId="placeholder"
          className="users-dropdown margin-top-20 margin-bottom-10"
          value={selectedUserId}
          onChange={(event: any) => setSelectedUserId(event?.target.value)}
          placeholder="Select new admin"
        >
          {users?.map((user, index) => (
            <MenuItem id={user.username} key={index} value={user.id}>
              {user.username}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      <Button
        disabled={!selectedUserId}
        onClick={addAdmin}
        className="margin-top-10 new-admin-button"
        variant="contained"
        color="secondary"
        data-testid={'button-' + props.buttonLabel}
      >
        {props.buttonLabel}
      </Button>
    </div>
  );
}
