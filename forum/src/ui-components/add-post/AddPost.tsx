import React, { useState, useEffect } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import './AddPost.css';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { Post } from '../../model/post';
import { User } from '../../model/user';

export default function AddPost(props: {
  postToBeEdited: Post;
  onAddPost: CallableFunction;
  user: User;
}) {
  const user = props.user;
  const [post, setPost] = useState<Post | null>(props.postToBeEdited);
  const [postContent, setPostContent] = useState<string>(props.postToBeEdited?.content);
  const onPostChange = (event: any): void => {
    setPost({ ...post, content: event.target.value } as Post);
    setPostContent(event.target.value);
  };

  useEffect(() => {
    setPost({ ...props.postToBeEdited });
    setPostContent(props.postToBeEdited?.content);
  }, [props.postToBeEdited]);

  const addPost = (): void => {
    props.onAddPost({ ...post });
    setPost(null);
    setPostContent('');
  };

  return (
    <div>
      <Card>
        <CardContent>
          <div className="flex">
            <img
              className="margin-top-10 avatar post-author"
              src={user.imageUrl}
              alt="Cannot display"
            />
            <div className="add-post">
              <form>
                <TextField
                  inputProps={{ 'data-testid': 'post' }}
                  value={postContent}
                  onChange={onPostChange}
                  className="add-post"
                  multiline={true}
                  id="outlined-basic"
                  variant="outlined"
                />
                <Button
                  data-testid="add"
                  disabled={!postContent}
                  onClick={addPost}
                  className="margin-top-10 float-right"
                  variant="contained"
                  color="primary"
                >
                  Add
                </Button>
              </form>
            </div>
          </div>
        </CardContent>
      </Card>
    </div>
  );
}
