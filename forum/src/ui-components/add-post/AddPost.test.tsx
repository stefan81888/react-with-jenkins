import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { unmountComponentAtNode } from 'react-dom';
import AddPost from './AddPost';
import { Post } from '../../model/post';
import { User } from '../../model/user';

describe('Add post component', () => {
  let container: any = null;
  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it('adds post', async () => {
    const addPostCallback = jest.fn();

    await act(async () => {
      render(
        <AddPost user={{} as User} postToBeEdited={{} as Post} onAddPost={addPostCallback} />,
        container
      );
    });

    const postInput = document.querySelector('[data-testid=post]');
    fireEvent.change(postInput as Element, { target: { value: 'sample post' } });
    const addPostButton = document.querySelector('[data-testid=add]');

    act(() => {
      addPostButton?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });

    expect(addPostCallback).toHaveBeenCalled();
  });
});
