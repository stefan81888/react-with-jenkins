import React from 'react';
import { render } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { unmountComponentAtNode } from 'react-dom';
import Subforum from './Subforum';
import { Subforum as SubforumModel } from '../../model/subforum';
import { Topic } from '../../model/topic';
import { User } from '../../model/user';

const subforumMock = {
  title: 'Title',
  topics: [
    {
      id: 0,
      title: 'title',
    } as Topic,
  ],
} as SubforumModel;
const removeTopicCallback = jest.fn();
const mockUser: User = {
  imageUrl: '',
  password: 'encrypted',
  username: 'stefan81888',
  isAdmin: true,
  isLoggedIn: true,
};

describe('Subforum component', () => {
  let container: any = null;
  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it('displays subforum title', async () => {
    await act(async () => {
      render(
        <Subforum subforum={subforumMock} removeTopic={removeTopicCallback} user={mockUser} />,
        container
      );
    });

    const subforumTitle = document.querySelector('[data-testid=title]');

    expect(subforumTitle).toBeTruthy();
    expect(subforumTitle?.textContent).toBe('Title');
  });

  it('removes topic if user is admin', async () => {
    await act(async () => {
      render(
        <Subforum subforum={subforumMock} removeTopic={removeTopicCallback} user={mockUser} />,
        container
      );
    });

    const expandCard = document.querySelector('[data-testid=card]');
    act(() => {
      expandCard?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });

    const topicToBeRemoved = document.querySelector('[data-testid=topic-0]');
    expect(topicToBeRemoved).toBeTruthy();

    act(() => {
      topicToBeRemoved?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });

    expect(removeTopicCallback).toHaveBeenCalledWith({
      id: 0,
      title: 'title',
    } as Topic);
  });
});
