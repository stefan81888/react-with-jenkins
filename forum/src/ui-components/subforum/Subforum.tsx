import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { Subforum as SubforumModel } from '../../model/subforum';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import './Subforum.css';
import { useHistory } from 'react-router-dom';
import DeleteTwoToneIcon from '@material-ui/icons/DeleteTwoTone';
import { Topic } from '../../model/topic';
import { User } from '../../model/user';

export default function Subforum(props: {
  subforum: SubforumModel;
  removeTopic: CallableFunction;
  user: User;
}) {
  const history = useHistory();
  const subforum = props.subforum;
  const user = props.user;
  const [expanded, setExpanded] = React.useState(false);
  const onExpandClick = () => {
    setExpanded(!expanded);
  };

  const goToTopic = (topicId: number): void => {
    history.push(`topic/${topicId}`);
  };

  const removeTopic = (topic: Topic): void => {
    props.removeTopic(topic);
  };

  return (
    <div className="full-width">
      <Card data-testid="card" onClick={onExpandClick} className="cursor-pointer">
        <CardContent className="float-left subforum-card-content">
          <div className="flex">
            <Typography color="textPrimary" gutterBottom={true} variant="h5" component="h2">
              <span data-testid="title">{subforum.title}</span>
            </Typography>
            <div className="collapse-icon">
              <IconButton onClick={onExpandClick} aria-expanded={expanded} aria-label="show more">
                <ExpandMoreIcon />
              </IconButton>
            </div>
          </div>
          <Collapse in={expanded} timeout="auto" unmountOnExit={true}>
            <List>
              {subforum.topics.map((topic, index) => (
                <ListItem className="topic" key={index}>
                  <ListItemText
                    data-testid={'topic-' + topic.title}
                    onClick={() => goToTopic(topic.id as number)}
                    primary={topic.title}
                  />
                  {user.isAdmin ? (
                    <p
                      id={topic.title}
                      data-testid={'topic-' + topic.id}
                      onClick={() => removeTopic(topic)}
                      className="margin-left-10"
                    >
                      <DeleteTwoToneIcon />
                    </p>
                  ) : null}
                </ListItem>
              ))}
            </List>
          </Collapse>
        </CardContent>
      </Card>
    </div>
  );
}
