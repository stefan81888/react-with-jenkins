import React, { createRef } from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import './Header.css';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { logout } from '../../store/login/login.actions';
import { User } from '../../model/user';

function Header(props: { user: User; logout: CallableFunction }) {
  const user = props.user;
  const history = useHistory();
  const wrapper = createRef<HTMLDivElement>();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const showOptions = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const closeOptions = (): void => {
    setAnchorEl(null);
  };

  const logUserOut = (): void => {
    props.logout();
    history?.push('/');
    setAnchorEl(null);
  };

  const navigateToLoginPage = (): void => {
    history.push('');
    setAnchorEl(null);
  };

  const goToHomepage = (): void => {
    history.push('/home');
    setAnchorEl(null);
  };

  const navigateToRegisterPage = (): void => {
    history.push('/register');
    setAnchorEl(null);
  };

  const navigateToAdminPage = (): void => {
    history.push('/admin');
    setAnchorEl(null);
  };

  const goToProfilePage = (): void => {
    history.push('/profile');
    setAnchorEl(null);
  };

  return (
    <div ref={wrapper}>
      <div className="options">
        <Button
          data-testid="home"
          className="float-left home-button"
          color="secondary"
          onClick={goToHomepage}
        >
          Home
        </Button>
        <Button
          data-testid="options"
          className="float-right options-button "
          color="primary"
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={showOptions}
        >
          Options
        </Button>
      </div>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted={true}
        open={Boolean(anchorEl)}
        onClose={closeOptions}
      >
        {user.isAdmin ? (
          <MenuItem data-testid="admin" onClick={navigateToAdminPage}>
            Admin panel
          </MenuItem>
        ) : null}
        {user.isLoggedIn ? (
          <div>
            {' '}
            <MenuItem data-testid="profile" onClick={() => goToProfilePage()}>
              Profile
            </MenuItem>{' '}
          </div>
        ) : null}
        {user.isLoggedIn ? (
          <div>
            {' '}
            <MenuItem data-testid="logout" onClick={logUserOut}>
              {' '}
              Logout{' '}
            </MenuItem>{' '}
          </div>
        ) : (
          <div>
            <MenuItem data-testid="login" onClick={navigateToLoginPage}>
              Login
            </MenuItem>
            <MenuItem data-testid="register" onClick={navigateToRegisterPage}>
              Register
            </MenuItem>
          </div>
        )}
      </Menu>
    </div>
  );
}

export { Header as HeaderComponent };

const mapStateToProps = (state: User) => {
  return {
    user: state,
  };
};

export default connect(mapStateToProps, { logout })(Header);
