import React from 'react';
import { render } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { unmountComponentAtNode } from 'react-dom';
import { HeaderComponent } from './Header';
import { User } from '../../model/user';

const isLoggedIn = true;
const logoutCallback = jest.fn();
const userMock = {
  isAdmin: true,
  isLoggedIn,
} as User;

describe('Header component', () => {
  let container: any = null;
  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it('displays header buttons', async () => {
    await act(async () => {
      render(<HeaderComponent user={userMock} logout={logoutCallback} />, container);
    });

    const homeButton = document.querySelector('[data-testid=home]');
    const optionsButton = document.querySelector('[data-testid=options]');

    expect(homeButton).toBeTruthy();
    expect(optionsButton).toBeTruthy();
  });

  it('displays admin panel option when admin is logged in', async () => {
    await act(async () => {
      render(<HeaderComponent user={userMock} logout={logoutCallback} />, container);
    });

    const adminMenuItem = document.querySelector('[data-testid=admin]');

    expect(adminMenuItem).toBeTruthy();
  });

  it('displays profile and logout option when user is logged in', async () => {
    await act(async () => {
      render(<HeaderComponent user={userMock} logout={logoutCallback} />, container);
    });

    const profileMenuItem = document.querySelector('[data-testid=profile]');
    const logoutMenuItem = document.querySelector('[data-testid=logout]');

    expect(profileMenuItem).toBeTruthy();
    expect(logoutMenuItem).toBeTruthy();
  });

  it('displays login and register option when user is not logged in', async () => {
    userMock.isLoggedIn = false;

    await act(async () => {
      render(<HeaderComponent user={userMock} logout={logoutCallback} />, container);
    });

    const loginMenuItem = document.querySelector('[data-testid=login]');
    const registerMenuItem = document.querySelector('[data-testid=register]');

    expect(loginMenuItem).toBeTruthy();
    expect(registerMenuItem).toBeTruthy();
  });

  it('logs user out', async () => {
    userMock.isLoggedIn = true;

    await act(async () => {
      render(<HeaderComponent user={userMock} logout={logoutCallback} />, container);
    });

    const logoutMenuItem = document.querySelector('[data-testid=logout]');
    expect(logoutMenuItem).toBeTruthy();

    act(() => {
      logoutMenuItem?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });

    expect(logoutCallback).toHaveBeenCalled();
  });
});
