import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Button from '@material-ui/core/Button';
import './ConfirmationModal.css';

export default function ConfirmationModal(props: {
  message: string;
  open: boolean;
  onConfirmationModalClose: any;
}) {
  return (
    <Dialog open={props.open} onClose={props.onConfirmationModalClose}>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          <span data-testid="confirmation-modal" className="flex flex-column">
            <span data-testid="message">{props.message}</span>
            <span className="flex confirmation-dialog-buttons">
              <Button
                data-testid="close"
                onClick={() => props.onConfirmationModalClose(false)}
                className="margin-h-10"
                variant="contained"
                color="secondary"
              >
                Close
              </Button>
              <Button
                data-testid="confirm"
                onClick={() => props.onConfirmationModalClose(true)}
                variant="contained"
                color="primary"
              >
                Confirm
              </Button>
            </span>
          </span>
        </DialogContentText>
      </DialogContent>
    </Dialog>
  );
}
