import React from 'react';
import { render } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { unmountComponentAtNode } from 'react-dom';
import ConfirmationModal from './ConfirmationModal';

const confirmationCallback = jest.fn();

describe('Confirmation modal component', () => {
  let container: any = null;
  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it('dispalys message in modal', async () => {
    await act(async () => {
      render(
        <ConfirmationModal
          message="Hello World!"
          onConfirmationModalClose={jest.fn()}
          open={true}
        />,
        container
      );
    });

    const message = document.querySelector('[data-testid=message]');

    expect(message?.textContent).toBe('Hello World!');
  });

  it('cancels confirmation', async () => {
    await act(async () => {
      render(
        <ConfirmationModal
          message="Hello World!"
          onConfirmationModalClose={confirmationCallback}
          open={true}
        />,
        container
      );
    });

    const closeButton = document.querySelector('[data-testid=close]');

    act(() => {
      closeButton?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });

    expect(confirmationCallback).toHaveBeenCalledWith(false);
  });

  it('confirms', async () => {
    await act(async () => {
      render(
        <ConfirmationModal
          message="Hello World!"
          onConfirmationModalClose={confirmationCallback}
          open={true}
        />,
        container
      );
    });

    const closeButton = document.querySelector('[data-testid=confirm]');

    act(() => {
      closeButton?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });

    expect(confirmationCallback).toHaveBeenCalledWith(true);
  });
});
