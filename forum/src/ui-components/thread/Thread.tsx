import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { Thread as ThreadModel } from '../../model/thread';
import DeleteTwoToneIcon from '@material-ui/icons/DeleteTwoTone';
import './Thread.css';

export default function Thread(props: {
  thread: ThreadModel;
  isUserAdmin: boolean;
  onDeleteThread: CallableFunction;
  onGoToThreadPost: CallableFunction;
}) {
  const thread = props.thread;

  return (
    <div>
      <Card className="cursor-pointer">
        <CardContent className="float-left thread-title">
          <div className="flex flex-column">
            <div data-testid="title">
              <span
                className="float-left"
                onClick={() => props.onGoToThreadPost(thread.id)}
                data-testid={'thread-' + thread.id}
              >
                {thread.title}
              </span>
              {props.isUserAdmin ? (
                <span
                  data-testid={'delete-thread-' + thread.title}
                  className="float-right delete-thread"
                  onClick={() => props.onDeleteThread(thread)}
                >
                  {' '}
                  <DeleteTwoToneIcon />
                </span>
              ) : null}
            </div>
          </div>
        </CardContent>
      </Card>
    </div>
  );
}
