import React from 'react';
import { render } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { unmountComponentAtNode } from 'react-dom';
import Thread from './Thread';
import { Thread as ThreadModel } from '../../model/thread';

describe('Thread component', () => {
  let container: any = null;
  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it('dispalys thread title', async () => {
    await act(async () => {
      render(
        <Thread
          isUserAdmin={false}
          onDeleteThread={jest.fn()}
          onGoToThreadPost={jest.fn()}
          thread={{ title: 'Test title' } as ThreadModel}
        />,
        container
      );
    });

    const title = document.querySelector('[data-testid=title]');

    expect(title?.textContent).toBe('Test title');
  });
});
