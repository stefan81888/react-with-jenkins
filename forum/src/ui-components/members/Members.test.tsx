import React from 'react';
import { render } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { unmountComponentAtNode } from 'react-dom';
import Members from './Members';
import { User } from '../../model/user';

describe('Members component', () => {
  let container: any = null;
  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it('dispalys forum members', async () => {
    await act(async () => {
      render(
        <Members users={[{ username: 'user' } as User, { username: 'test' } as User]} />,
        container
      );
    });

    const members = document.querySelector('[data-testid=members]');

    expect(members?.textContent).toBe('user, test');
  });
});
