import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { User } from '../../model/user';
import Typography from '@material-ui/core/Typography';

export default function Members(props: { users: User[] }) {
  const users = props.users.map((user) => user.username).join(', ');

  return (
    <div>
      <Card className="cursor-pointer">
        <CardContent className="float-left flex flex-column">
          <Typography gutterBottom={true} variant="h5" component="h2">
            Forum members
          </Typography>
          <div className="flex flex-column">
            <div data-testid="members">{users}</div>
          </div>
        </CardContent>
      </Card>
    </div>
  );
}
