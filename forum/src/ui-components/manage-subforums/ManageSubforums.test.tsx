import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { unmountComponentAtNode } from 'react-dom';
import ManageSubforums from './ManageSubforums';

const addSubforumCallback = jest.fn();
const removeSubforumCallback = jest.fn();
const subforumsMock = [
  { id: 0, title: 'test', topics: [] },
  { id: 0, title: 'another-subforum', topics: [] },
];

describe('Manage subforums component', () => {
  let container: any = null;
  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it('adds subforum', async () => {
    await act(async () => {
      render(
        <ManageSubforums
          subforums={[]}
          addSubforum={addSubforumCallback}
          removeSubforum={removeSubforumCallback}
        />,
        container
      );
    });

    const subforumNameInput = document.querySelector('[data-testid=subforum-name]');
    fireEvent.change(subforumNameInput as Element, { target: { value: 'test' } });
    const addSubfroumButton = document.querySelector('[data-testid=add-subforum]');

    act(() => {
      addSubfroumButton?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });

    expect(addSubforumCallback).toHaveBeenCalled();
  });

  it('displays subforums', async () => {
    await act(async () => {
      render(
        <ManageSubforums
          subforums={subforumsMock}
          addSubforum={addSubforumCallback}
          removeSubforum={removeSubforumCallback}
        />,
        container
      );
    });

    const firstSubforum = document.querySelector('[data-testid=test]');
    expect(firstSubforum).toBeTruthy();
    const secondSubforum = document.querySelector('[data-testid=another-subforum]');
    expect(secondSubforum).toBeTruthy();
  });

  it('removes subforum', async () => {
    await act(async () => {
      render(
        <ManageSubforums
          subforums={subforumsMock}
          addSubforum={addSubforumCallback}
          removeSubforum={removeSubforumCallback}
        />,
        container
      );
    });

    const removeSubfroumButton = document.querySelector('[data-testid=remove-test]');

    act(() => {
      removeSubfroumButton?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });

    const confirmationModal = document.querySelector('[data-testid=confirmation-modal]');

    expect(confirmationModal).toBeTruthy();
  });
});
