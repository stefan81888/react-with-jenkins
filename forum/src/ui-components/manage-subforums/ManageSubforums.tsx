import React, { useState, useEffect } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import './ManageSubforums.css';
import { Subforum } from '../../model/subforum';
import Divider from '@material-ui/core/Divider';
import NotificationModal from '../notification-modal/NotificationModal';
import DeleteTwoToneIcon from '@material-ui/icons/DeleteTwoTone';
import ConfirmationModal from '../confirmation-modal/ConfirmationModal';

export default function ManageSubforums(props: {
  subforums: Subforum[];
  addSubforum: CallableFunction;
  removeSubforum: CallableFunction;
}) {
  const [newSubforumTitle, setNewSubforumTitle] = useState<string>('');
  const [subforums, setSubfroums] = useState<Subforum[]>(props.subforums);
  const [isNotificationModalOpen, setIsNotificationModalOpen] = useState<boolean>(false);
  const [isConfirmationModalOpen, setIsConfirmationModalOpen] = useState<boolean>(false);
  const [subforumForDeletion, setSubfroumForDeletion] = useState<Subforum | null>(null);

  const onSubforumTitleChange = (event: any): void => {
    setNewSubforumTitle(event.target.value);
  };

  const onNotificationModalClose = (): void => {
    setIsNotificationModalOpen(false);
  };

  const removeSubforum = (subforum: Subforum): void => {
    props.removeSubforum(subforum);
  };

  const addSubforum = (): void => {
    const subforumExists = subforums.find((x) => x.title === newSubforumTitle);

    if (subforumExists) {
      setIsNotificationModalOpen(true);
    } else {
      props.addSubforum(newSubforumTitle);
    }

    setNewSubforumTitle('');
  };

  const onDeletionConfirmed = (confirmed: boolean): void => {
    if (confirmed && subforumForDeletion) {
      removeSubforum(subforumForDeletion);
    }

    setIsConfirmationModalOpen(false);
    setSubfroumForDeletion(null);
  };

  useEffect(() => {
    setSubfroums(props.subforums);
  }, [props.subforums]);

  return (
    <div className="flex flex-column">
      <Card>
        <CardContent className="flex flex-column">
          <h2>Manage subforums</h2>
          <Divider className="margin-top-10 margin-bottom-10" />
          <div className="flex">
            <form className="flex flex-column add-subforum-form">
              <TextField
                inputProps={{ 'data-testid': 'subforum-name' }}
                className="float-left margin-bottom-10 subforum-name-input"
                value={newSubforumTitle}
                onChange={onSubforumTitleChange}
                label="Subfroum name"
              />
              <Button
                data-testid="add-subforum"
                disabled={!newSubforumTitle}
                className="margin-top-10 add-subforum-button"
                variant="contained"
                color="primary"
                onClick={addSubforum}
              >
                Add subforum
              </Button>
            </form>
            <Divider className="subforum-separator" orientation="vertical" flexItem={true} />
            <div className="subforums-list">
              <h4>Subforums</h4>
              {subforums.map((subforum, index) => (
                <div data-testid={subforum.title} className="flex subforums-list" key={index}>
                  <p className="subforum-title">{subforum.title}</p>
                  <p
                    data-testid={'remove-' + subforum.title}
                    onClick={() => {
                      setSubfroumForDeletion(subforum);
                      setIsConfirmationModalOpen(true);
                    }}
                    className="margin-left-10 cursor-pointer"
                  >
                    <DeleteTwoToneIcon />
                  </p>
                </div>
              ))}
            </div>
          </div>
        </CardContent>
      </Card>
      <NotificationModal
        onNotificationModalClose={onNotificationModalClose}
        open={isNotificationModalOpen}
        message="Subforum already exists"
      />
      <ConfirmationModal
        onConfirmationModalClose={(confirmed: any) => onDeletionConfirmed(confirmed)}
        open={isConfirmationModalOpen}
        message="Are you sure you want to delete this subforum?"
      />
    </div>
  );
}
