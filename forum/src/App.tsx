import React from 'react';
import './App.css';
import Homepage from './pages/homepage/Homepage';
import { HashRouter as Router, Switch } from 'react-router-dom';
import Login from './pages/login/Login';
import Header from './ui-components/header/Header';
import Topic from './pages/topic/Topic';
import ThreadPosts from './pages/thread-posts/ThreadPosts';
import Profile from './pages/profile/Profile';
import Register from './pages/register/Register';
import { SubforumService } from './data-access/subforums';
import { TopicService } from './data-access/topic';
import { UsersService } from './data-access/users';
import Admin from './pages/admin/Admin';
import { PostsService } from './data-access/posts';
import { ThreadService } from './data-access/thread';

import { GuardProvider, GuardedRoute } from 'react-router-guards';
import { adminGuard } from './guards/admin.guard';
import { authGuard } from './guards/auth.guard';
import EditUser from './pages/edit-user/EditUser';

function App() {
  const userService = new UsersService();
  const postsService = new PostsService();
  const threadsService = new ThreadService();
  const topicsService = new TopicService();
  const subforumsService = new SubforumService();

  return (
    <div className="app flex flex-column">
      <Router>
        <GuardProvider guards={[adminGuard, authGuard]}>
          <div data-testid="header">
            <Header />
          </div>
          <div className="content">
            <Switch>
              <GuardedRoute
                exact={true}
                path="/"
                component={() => <Login usersService={userService} />}
              />
              <GuardedRoute
                exact={true}
                path="/register"
                component={() => <Register usersService={userService} />}
              />
              <GuardedRoute
                exact={true}
                path="/home"
                component={() => (
                  <Homepage
                    usersService={userService}
                    subforumService={subforumsService}
                    topicService={topicsService}
                  />
                )}
              />
              <GuardedRoute
                exact={true}
                path="/profile"
                meta={{ auth: true }}
                component={() => (
                  <Profile postsService={postsService} threadService={threadsService} />
                )}
              />
              <GuardedRoute
                exact={true}
                path="/topic/:topicId"
                component={(props: any) => (
                  <Topic
                    {...props}
                    postsService={postsService}
                    threadService={threadsService}
                    topicsService={topicsService}
                  />
                )}
              />
              <GuardedRoute
                exact={true}
                path="/thread/:threadId"
                component={(props: any) => (
                  <ThreadPosts
                    {...props}
                    postsService={postsService}
                    threadService={threadsService}
                  />
                )}
              />
              <GuardedRoute
                exact={true}
                path="/admin"
                meta={{ admin: true }}
                component={() => (
                  <Admin subforumsService={subforumsService} usersService={userService} />
                )}
              />
              <GuardedRoute
                exact={true}
                path="/edit-user"
                meta={{ auth: true }}
                component={() => <EditUser usersService={userService} />}
              />
            </Switch>
          </div>
        </GuardProvider>
      </Router>
    </div>
  );
}

export default App;
