import { createStore } from 'redux';
import { loginReducer } from './login/login.reducer';

export const store = createStore(loginReducer);
