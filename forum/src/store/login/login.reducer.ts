import { User } from '../../model/user';
import { LoginAction, LOGIN, LOGOUT, LOGGED_USER } from './login.actions';
import { LoginService } from '../../services/login.service';

const loginService = new LoginService();
const initialUserState: User = {} as User;

export function loginReducer(state: User = initialUserState, action: LoginAction): User {
  switch (action.type) {
    case LOGIN:
      const user = action.user as User;
      loginService.logUserIn(user);
      return { ...user, isLoggedIn: true };

    case LOGOUT:
      loginService.logUserOut();
      return { ...initialUserState, isLoggedIn: false };

    case LOGGED_USER:
      const userInStorage = loginService.getLoggedUser();
      return { ...userInStorage, isLoggedIn: true };

    default:
      return state;
  }
}
