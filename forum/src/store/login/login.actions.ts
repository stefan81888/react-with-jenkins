import { User } from '../../model/user';

export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const LOGGED_USER = 'LOGGED_USER';

export function login(userForLogin: User): LoginAction {
  return {
    type: LOGIN,
    user: userForLogin,
  };
}

export function logout(): LoginAction {
  return {
    type: LOGOUT,
  };
}

export function user(): LoginAction {
  return {
    type: LOGGED_USER,
  };
}

export interface LoginAction {
  type: string;
  user?: User;
}
