import React from 'react';
import { render } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { unmountComponentAtNode } from 'react-dom';
import { EditUserComponent } from './EditUser';
import { UsersService } from '../../data-access/users';
import { User } from '../../model/user';

const userMock: User = {
  isAdmin: true,
  isLoggedIn: true,
} as User;
const usersServiceMock = ({
  getUsers: () => {
    return Promise.resolve([]);
  },
} as unknown) as UsersService;

describe('Edit user page', () => {
  let container: any = null;
  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it('shows form for editing user', async () => {
    await act(async () => {
      render(
        <EditUserComponent
          user={userMock}
          login={jest.fn()}
          logout={jest.fn()}
          usersService={usersServiceMock}
        />,
        container
      );
    });

    const userForm = document.querySelector('[data-testid=add-user]');

    expect(userForm).toBeTruthy();
  });
});
