import React from 'react';
import { UsersService } from '../../data-access/users';
import AddUser from '../../ui-components/add-user/AddUser';
import { User } from '../../model/user';
import { useHistory } from 'react-router';
import { connect } from 'react-redux';
import { login, logout } from '../../store/login/login.actions';

function EditUser(props: {
  usersService: UsersService;
  user: User;
  login: CallableFunction;
  logout: CallableFunction;
}) {
  const history = useHistory();

  const editUser = (user: User): void => {
    props.usersService.editUser(user).then((editedUser: User) => {
      props.logout();
      props.login(editedUser);
      history.push('profile');
    });
  };

  return (
    <div className="flex flex-column flex-center">
      <div data-testid="add-user">
        <AddUser user={props.user} buttonLabel="Edit" onAddUser={(user: User) => editUser(user)} />
      </div>
    </div>
  );
}

export { EditUser as EditUserComponent };

const mapStateToProps = (state: User) => {
  return {
    user: state,
  };
};

export default connect(mapStateToProps, { login, logout })(EditUser);
