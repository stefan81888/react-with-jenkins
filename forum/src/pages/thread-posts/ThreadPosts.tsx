import React, { useState, useEffect, useRef } from 'react';
import { PostsService } from '../../data-access/posts';
import { Post as PostModel } from '../../model/post';
import Post from '../../ui-components/post/Post';
import AddPost from '../../ui-components/add-post/AddPost';
import { ThreadService } from '../../data-access/thread';
import { Thread } from '../../model/thread';
import { User } from '../../model/user';
import { connect } from 'react-redux';

function ThreadPosts(props: {
  postsService: PostsService;
  threadService: ThreadService;
  user: User;
  match?: any;
}) {
  const postsService = props.postsService;
  const threadService = props.threadService;
  const user = props.user;
  const [posts, setPosts] = useState<PostModel[]>([]);
  const [thread, setThread] = useState<Thread>();
  const [postToBeEdited, setPost] = useState<PostModel | null>(null);
  const addPostRef = useRef<null | HTMLDivElement>(null);

  useEffect(() => {
    loadPosts();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    threadService.getThreads().then((threads: Thread[]) => {
      const foundThread = threads.find((x) => x.id === parseInt(props.match.params.threadId, 10));
      setThread(foundThread);
    });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const loadPosts = () => {
    postsService.getPosts().then((allPosts: PostModel[]) => {
      const threadPosts = allPosts.filter(
        (post) => post.threadId === parseInt(props.match.params.threadId, 10)
      );
      setPosts(threadPosts);
    });
  };

  const addPost = (changedPost: PostModel) => {
    const post = {
      content: changedPost.content,
      id: changedPost.id,
      threadId: parseInt(props.match.params.threadId, 10),
      user,
    };

    if (!post.id) {
      setPosts([...posts, post]);
      postsService.addPost(post).then(() => loadPosts());
    } else {
      postsService.editPost(post).then(() => loadPosts());
    }

    setPost(null);
  };

  const onPostEdit = (initialPost: PostModel) => {
    setPost({ ...initialPost });
    addPostRef?.current?.scrollIntoView({ behavior: 'smooth', block: 'start' });
  };

  const onPostDelete = (postToBeDeleted: PostModel) => {
    postsService.deletePost(postToBeDeleted).then(() => loadPosts());
  };

  return (
    <div className="flex flex-column">
      <div>
        <h2 data-testid="thread-title" className="float-left margin-left-10">
          {thread?.title} thread
        </h2>
      </div>
      {posts.map((post, index) => (
        <div
          id={post.content}
          data-testid={'post-' + post.id}
          key={index}
          className="margin-top-10"
        >
          <Post
            user={user}
            onPostEdit={(initialPost: PostModel) => onPostEdit(initialPost)}
            onPostDelete={(postToBeDeleted: PostModel) => onPostDelete(postToBeDeleted)}
            post={post}
          />
        </div>
      ))}
      <hr className="margin-top-10" />
      <div ref={addPostRef} className="margin-top-10">
        {user.isLoggedIn ? (
          <div data-testid="add-post">
            <AddPost
              user={user}
              postToBeEdited={postToBeEdited as PostModel}
              onAddPost={(changedPost: PostModel) => addPost(changedPost)}
            />
          </div>
        ) : null}
      </div>
    </div>
  );
}

export { ThreadPosts as ThreadPostsComponent };

const mapStateToProps = (state: User) => {
  return {
    user: state,
  };
};

export default connect(mapStateToProps, {})(ThreadPosts);
