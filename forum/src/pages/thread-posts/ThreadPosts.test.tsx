import React from 'react';
import { render } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { unmountComponentAtNode } from 'react-dom';
import { ThreadPostsComponent } from './ThreadPosts';
import { Post } from '../../model/post';
import { PostsService } from '../../data-access/posts';
import { Thread } from '../../model/thread';
import { ThreadService } from '../../data-access/thread';
import { User } from '../../model/user';

const postsServiceMock = ({
  getPosts: () => {
    return Promise.resolve([
      {
        id: 0,
        content: 'post',
        threadId: 0,
        user: {
          username: 'admin',
        },
      } as Post,
      {
        id: 1,
        threadId: 0,
        content: 'some-more-content',
        user: {
          username: 'admin',
        },
      } as Post,
    ]);
  },
} as unknown) as PostsService;
const threadsServiceMock = ({
  getThreads: () => {
    return Promise.resolve([
      {
        id: 0,
        title: 'Thread title',
      } as Thread,
    ]);
  },
} as unknown) as ThreadService;
const userMock: User = {
  isAdmin: true,
  isLoggedIn: true,
} as User;

describe('Thread posts page', () => {
  let container: any = null;
  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it('shows thread title', async () => {
    await act(async () => {
      render(
        <ThreadPostsComponent
          match={{
            params: {
              threadId: 0,
            },
          }}
          postsService={postsServiceMock}
          threadService={threadsServiceMock}
          user={userMock}
        />,
        container
      );
    });

    const title = document.querySelector('[data-testid=thread-title]');

    expect(title).toBeTruthy();
    expect(title?.textContent).toBe('Thread title thread');
  });

  it('renders thread posts', async () => {
    await act(async () => {
      render(
        <ThreadPostsComponent
          match={{
            params: {
              threadId: 0,
            },
          }}
          postsService={postsServiceMock}
          threadService={threadsServiceMock}
          user={userMock}
        />,
        container
      );
    });

    const firstPost = document.querySelector('[data-testid=post-0]');
    const secondPost = document.querySelector('[data-testid=post-1]');

    expect(firstPost).toBeTruthy();
    expect(secondPost).toBeTruthy();
  });

  it('allows user to add post if user is logged in', async () => {
    await act(async () => {
      render(
        <ThreadPostsComponent
          match={{
            params: {
              threadId: 0,
            },
          }}
          postsService={postsServiceMock}
          threadService={threadsServiceMock}
          user={userMock}
        />,
        container
      );
    });

    const addPost = document.querySelector('[data-testid=add-post]');

    expect(addPost).toBeTruthy();
  });

  it('does not allow user to add post if user is not logged in', async () => {
    userMock.isLoggedIn = false;

    await act(async () => {
      render(
        <ThreadPostsComponent
          match={{
            params: {
              threadId: 0,
            },
          }}
          postsService={postsServiceMock}
          threadService={threadsServiceMock}
          user={userMock}
        />,
        container
      );
    });

    const addPost = document.querySelector('[data-testid=add-post]');

    expect(addPost).toBeFalsy();
  });
});
