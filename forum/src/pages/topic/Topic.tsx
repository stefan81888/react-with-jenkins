import React, { useState, useEffect } from 'react';
import { Thread as ThreadModel } from '../../model/thread';
import { ThreadService } from '../../data-access/thread';
import Thread from '../../ui-components/thread/Thread';
import { TopicService } from '../../data-access/topic';
import { Topic as TopicModel } from '../../model/topic';
import Button from '@material-ui/core/Button';
import './Topic.css';
import Dialog from '@material-ui/core/Dialog';
import TextField from '@material-ui/core/TextField';
import { PostsService } from '../../data-access/posts';
import { useHistory } from 'react-router';
import { User } from '../../model/user';
import { connect } from 'react-redux';

function Topic(props: {
  postsService: PostsService;
  threadService: ThreadService;
  topicsService: TopicService;
  user: User;
  match?: any;
}) {
  const threadService = props.threadService;
  const topicService = props.topicsService;
  const postsService = props.postsService;
  const user = props.user;
  const [threads, setThreads] = useState<ThreadModel[]>([]);
  const [topicId] = useState<number>(parseInt(props.match.params.topicId, 10));
  const [topic, setTopic] = useState<TopicModel>();
  const [isAddThreadDialogOpen, setIsAddThreadDialogOpen] = useState<boolean>(false);
  const [newThreadName, setNewThreadName] = useState<string>('');
  const [initialPostContent, setPost] = useState<string>('');
  const history = useHistory();

  const goToThreadPosts = (threadId: number): void => {
    history.push(`/thread/${threadId}`);
  };

  useEffect(() => {
    loadThread();

    topicService.getTopicById(topicId).then((foundTopic) => setTopic(foundTopic));
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const addThread = () => {
    const newThread = {
      title: newThreadName,
      topicId,
    };
    threadService.addThread(newThread).then((thread) => {
      postsService.addPost({
        content: initialPostContent,
        threadId: thread.id as number,
        id: null,
        user,
      });
      setThreads([...threads, thread]);
    });
    setNewThreadName('');
    setPost('');
    setIsAddThreadDialogOpen(false);
  };

  const onAddThreadDialogClose = (): void => {
    setIsAddThreadDialogOpen(false);
  };

  const onThreadNameChange = (event: any): void => {
    setNewThreadName(event.target.value);
  };

  const onPostChange = (event: any): void => {
    setPost(event.target.value);
  };

  const removeThread = (thread: ThreadModel): void => {
    threadService.removeThread(thread).then(() => loadThread());
  };

  const loadThread = () => {
    threadService.getThreads().then((allThreads: ThreadModel[]) => {
      const topicThreads = allThreads.filter((thread) => thread.topicId === topicId);
      setThreads(topicThreads);
    });
  };

  return (
    <div className="flex flex-column">
      <div className="margin-bottom-10">
        <h2 className="float-left">{topic?.title} topic</h2>
        <div className="float-right add-thread">
          {user.isLoggedIn ? (
            <Button
              data-testid="new-thread-button"
              onClick={() => setIsAddThreadDialogOpen(true)}
              variant="contained"
              color="primary"
            >
              New thread
            </Button>
          ) : null}
        </div>
      </div>
      {threads.map((thread, index) => (
        <div key={index}>
          <Thread
            onGoToThreadPost={() => goToThreadPosts(thread.id as number)}
            onDeleteThread={(threadToBeRemoved: ThreadModel) => removeThread(threadToBeRemoved)}
            isUserAdmin={user.isAdmin}
            thread={thread}
          />
        </div>
      ))}
      <Dialog open={isAddThreadDialogOpen} onClose={onAddThreadDialogClose}>
        <div className="add-thread-dialog">
          <form className="flex flex-column">
            <TextField
              inputProps={{ 'data-testid': 'new-topic-name' }}
              className="add-thread-input margin-top-10"
              value={newThreadName}
              onChange={onThreadNameChange}
              label="Thread name"
            />
            <TextField
              inputProps={{ 'data-testid': 'post' }}
              value={initialPostContent}
              onChange={onPostChange}
              className="add-thread-input margin-top-10"
              multiline={true}
              id="outlined-basic"
              label="Initial post"
              variant="outlined"
            />
            <Button
              data-testid="add-thread"
              disabled={!newThreadName || !initialPostContent}
              onClick={addThread}
              className="add-thread-buttom margin-top-10 margin-bottom-10"
              variant="contained"
              color="primary"
            >
              Add thread
            </Button>
          </form>
        </div>
      </Dialog>
    </div>
  );
}

export { Topic as TopicComponent };

const mapStateToProps = (state: User) => {
  return {
    user: state,
  };
};

export default connect(mapStateToProps, {})(Topic);
