import React from 'react';
import { render } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { unmountComponentAtNode } from 'react-dom';
import { TopicComponent } from './Topic';
import { Post } from '../../model/post';
import { PostsService } from '../../data-access/posts';
import { Thread } from '../../model/thread';
import { ThreadService } from '../../data-access/thread';
import { TopicService } from '../../data-access/topic';
import { Topic as TopicModel } from '../../model/topic';
import { User } from '../../model/user';

const postsServiceMock = ({
  getPosts: () => {
    return Promise.resolve([
      {
        id: 0,
        content: 'post',
        threadId: 0,
        user: {
          username: 'admin',
        },
      } as Post,
      {
        id: 1,
        threadId: 0,
        content: 'some-more-content',
        user: {
          username: 'admin',
        },
      } as Post,
    ]);
  },
} as unknown) as PostsService;
const threadsServiceMock = ({
  getThreads: () => {
    return Promise.resolve([
      {
        id: 0,
        topicId: 0,
        title: 'Thread title',
      } as Thread,
    ]);
  },
} as unknown) as ThreadService;
const userMock: User = {
  isAdmin: true,
  isLoggedIn: true,
} as User;
const topicServiceMock: TopicService = {
  addTopic: (topic: TopicModel) => {
    return new Promise(() => {
      return {} as TopicModel;
    });
  },
  getTopicById: (topicId: number) => {
    return Promise.resolve({
      id: 0,
      title: 'Topic title',
    } as TopicModel);
  },
} as TopicService;

describe('Thread posts page', () => {
  let container: any = null;
  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it('displays threads of a topic', async () => {
    await act(async () => {
      render(
        <TopicComponent
          match={{
            params: {
              topicId: 0,
            },
          }}
          user={userMock}
          postsService={postsServiceMock}
          threadService={threadsServiceMock}
          topicsService={topicServiceMock}
        />,
        container
      );
    });

    const thread = document.querySelector('[data-testid=thread-0]');

    expect(thread).toBeTruthy();
  });

  it('displays new thread button when user is logged in', async () => {
    await act(async () => {
      render(
        <TopicComponent
          match={{
            params: {
              topicId: 0,
            },
          }}
          user={userMock}
          postsService={postsServiceMock}
          threadService={threadsServiceMock}
          topicsService={topicServiceMock}
        />,
        container
      );
    });

    const button = document.querySelector('[data-testid=new-thread-button]');

    expect(button).toBeTruthy();
  });
});
