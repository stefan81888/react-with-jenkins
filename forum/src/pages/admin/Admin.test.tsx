import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { unmountComponentAtNode } from 'react-dom';
import Admin from './Admin';
import { SubforumService } from '../../data-access/subforums';
import { UsersService } from '../../data-access/users';
import { Subforum } from '../../model/subforum';

const usersServiceMock = ({
  getUsers: () => {
    return Promise.resolve([]);
  },
} as unknown) as UsersService;
const subforumsServiceMock: SubforumService = ({
  getSubforums: () => {
    return Promise.resolve([]);
  },
  editSubforum: () => {
    return new Promise(() => {
      return {} as Subforum;
    });
  },
} as unknown) as SubforumService;

describe('Admin page', () => {
  let container: any = null;
  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it('shows panel for managing subforums', async () => {
    await act(async () => {
      render(
        <Admin subforumsService={subforumsServiceMock} usersService={usersServiceMock} />,
        container
      );
    });

    const manageSubforumsPanel = document.querySelector('[data-testid=manage-subforums]');

    expect(manageSubforumsPanel).toBeTruthy();
  });

  it('shows panel for managing users', async () => {
    await act(async () => {
      render(
        <Admin subforumsService={subforumsServiceMock} usersService={usersServiceMock} />,
        container
      );
    });

    const mamangeUsersPanel = document.querySelector('[data-testid=manage-users]');

    expect(mamangeUsersPanel).toBeTruthy();
  });
});
