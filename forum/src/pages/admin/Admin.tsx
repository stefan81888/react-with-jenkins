import React, { useState, useEffect } from 'react';
import ManageSubforums from '../../ui-components/manage-subforums/ManageSubforums';
import { SubforumService } from '../../data-access/subforums';
import { Subforum } from '../../model/subforum';
import ManageUsers from '../../ui-components/manage-users/ManageUsers';
import { User } from '../../model/user';
import { UsersService } from '../../data-access/users';

export default function Admin(props: {
  subforumsService: SubforumService;
  usersService: UsersService;
}) {
  const subforumsService = props.subforumsService;
  const usersService = props.usersService;

  const [subforums, setSubforums] = useState<Subforum[]>([]);
  const [users, setUsers] = useState<User[]>([]);

  const addSubforums = (subforumName: string): void => {
    subforumsService
      .addSubforum({
        title: subforumName,
        topics: [],
      })
      .then((addedSubfroum) => {
        setSubforums([...subforums, addedSubfroum]);
      });
  };

  const removeSubforum = (subforum: Subforum): void => {
    subforumsService.removeSubforum(subforum).then(() => {
      subforumsService.getSubforums().then((allSubforums: Subforum[]) => {
        setSubforums(allSubforums);
      });
    });
  };

  useEffect(() => {
    loadUsers();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    subforumsService.getSubforums().then((allSubforums: Subforum[]) => {
      setSubforums(allSubforums);
    });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const addAdminUser = (user: User): void => {
    usersService.addUser(user).then(() => loadUsers());
  };

  const editUser = (user: User): void => {
    usersService.editUser(user).then(() => loadUsers());
  };

  const deleteUser = (user: User): void => {
    usersService.deleteUser(user).then(() => loadUsers());
  };

  const loadUsers = (): void => {
    usersService.getUsers().then((allUsers: User[]) => {
      setUsers(allUsers);
    });
  };

  return (
    <div className="flex flex-column">
      <div data-testid="manage-subforums">
        <ManageSubforums
          subforums={subforums}
          addSubforum={(subforumName: string) => addSubforums(subforumName)}
          removeSubforum={(subforum: Subforum) => removeSubforum(subforum)}
        />
      </div>
      <div data-testid="manage-users" className="margin-top-10">
        <ManageUsers
          users={users}
          addAdmin={(user: User) => addAdminUser(user)}
          editUser={(newAdmin: User) => editUser(newAdmin)}
          deleteUser={(userToBeDeleted: User) => deleteUser(userToBeDeleted)}
        />
      </div>
    </div>
  );
}
