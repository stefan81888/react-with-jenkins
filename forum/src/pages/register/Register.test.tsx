import React from 'react';
import { render } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { unmountComponentAtNode } from 'react-dom';
import { UsersService } from '../../data-access/users';
import { User } from '../../model/user';
import { RegisterComponent } from './Register';

const usersMock: User[] = [];

const userServiceMock = ({
  getUsers: () => {
    return Promise.resolve(usersMock);
  },
} as unknown) as UsersService;

describe('Register page', () => {
  let container: any = null;
  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it('Displays add user component', async () => {
    await act(async () => {
      render(<RegisterComponent login={jest.fn()} usersService={userServiceMock} />, container);
    });

    const addUserComponent = document.querySelector('[data-testid=add]');
    expect(addUserComponent).toBeTruthy();
  });
});
