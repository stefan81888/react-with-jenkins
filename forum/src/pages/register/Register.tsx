import React, { useState, useEffect } from 'react';
import { User } from '../../model/user';
import { UsersService } from '../../data-access/users';
import { useHistory } from 'react-router-dom';
import NotificationModal from '../../ui-components/notification-modal/NotificationModal';
import AddUser from '../../ui-components/add-user/AddUser';
import { login } from '../../store/login/login.actions';
import { connect } from 'react-redux';

function Register(props: { usersService: UsersService; login: CallableFunction }) {
  const usersService = props.usersService;
  const history = useHistory();

  const [users, setUsers] = useState<User[]>([]);
  const [isNotificationModalOpen, setIsNotificationModalOpen] = useState<boolean>(false);

  useEffect(() => {
    usersService.getUsers().then((allUsers: User[]) => {
      setUsers(allUsers);
    });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const register = (user: User): void => {
    const userFromDb = users.find((u) => u.username === user.username);
    if (userFromDb) {
      setIsNotificationModalOpen(true);
    } else {
      usersService.addUser(user).then((newUser) => {
        props.login(newUser);
        history.push('/home');
      });
    }
  };

  const onNotificationModalClose = (): void => {
    setIsNotificationModalOpen(false);
  };

  return (
    <div className="flex flex-center">
      <div data-testid="add">
        <AddUser buttonLabel="Register" onAddUser={(user: User) => register(user)} />
      </div>
      <NotificationModal
        onNotificationModalClose={onNotificationModalClose}
        open={isNotificationModalOpen}
        message="Username already exists"
      />
    </div>
  );
}

export { Register as RegisterComponent };

export default connect(
  () => {
    return {};
  },
  { login }
)(Register);
