import React from 'react';
import { render } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { unmountComponentAtNode } from 'react-dom';
import { ProfileComponent } from './Profile';
import { PostsService } from '../../data-access/posts';
import { ThreadService } from '../../data-access/thread';
import { Post } from '../../model/post';
import { Thread } from '../../model/thread';
import { User } from '../../model/user';

const postsServiceMock = ({
  getPosts: () => {
    return Promise.resolve([
      {
        content: 'post',
        user: {
          username: 'admin',
        },
      } as Post,
      {
        content: 'some-more-content',
        user: {
          username: 'admin',
        },
      } as Post,
    ]);
  },
} as unknown) as PostsService;
const threadsServiceMock = ({
  getThreads: () => {
    return Promise.resolve([{} as Thread]);
  },
} as unknown) as ThreadService;

const userMock: User = {
  isAdmin: true,
  isLoggedIn: true,
} as User;

describe('Profile page', () => {
  let container: any = null;
  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it('Displays user profile', async () => {
    await act(async () => {
      render(
        <ProfileComponent
          user={userMock}
          threadService={threadsServiceMock}
          postsService={postsServiceMock}
        />,
        container
      );
    });

    const posts = document.querySelectorAll('.post');
    expect(posts).toBeTruthy();
  });
});
