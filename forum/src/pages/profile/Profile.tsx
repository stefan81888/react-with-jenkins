import React, { useState, useEffect } from 'react';
import { User } from '../../model/user';
import { Post } from '../../model/post';
import { PostsService } from '../../data-access/posts';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import './Profile.css';
import Typography from '@material-ui/core/Typography';
import { Thread } from '../../model/thread';
import { ThreadService } from '../../data-access/thread';
import { useHistory } from 'react-router';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';

function Profile(props: { threadService: ThreadService; postsService: PostsService; user: User }) {
  const threadService = props.threadService;
  const postsService = props.postsService;

  const history = useHistory();
  const [user] = useState<User>(props.user);
  const [userPosts, setUserPosts] = useState<{ post: Post; thread: Thread }[]>([]);

  useEffect(() => {
    postsService.getPosts().then((allPosts) => {
      const posts = allPosts.filter((post) => post.user.id === user.id);
      threadService.getThreads().then((threads: Thread[]) => {
        const postsWithThread: { post: Post; thread: Thread }[] = [];
        posts.forEach((post) => {
          const thread = threads.find((x) => x.id === post.threadId);
          postsWithThread.push({
            post,
            thread: thread as Thread,
          });
        });
        setUserPosts(postsWithThread);
      });
    });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const goToThread = (threadId: number): void => {
    history.push(`/thread/${threadId}`);
  };

  return (
    <div className="flex flex-column">
      <div className="profile-image">
        <h2 className="float-left">{user.username} profile</h2>
      </div>
      <Card className="margin-top-10">
        <div className="flex margin-top-10">
          <div className="profile-summary">
            <div>
              <img className="margin-left-10 profile-image" src={user.imageUrl} alt="avatar" />
            </div>
            <div className="flex flex-column flex-center margin-top-10">
              <div data-testid="username">{user.username}</div>
              <div>{userPosts.length} posts</div>
              <div>
                <Button
                  data-testid="edit-profile-button"
                  className="margin-bottom-10"
                  onClick={() => {
                    history.push('edit-user');
                  }}
                  color="secondary"
                >
                  Edit profile
                </Button>
              </div>
            </div>
          </div>
          <div className="flex flex-column user-posts">
            {userPosts.map((postAndThread, index) => (
              <Card key={index} className="margin-top-10 margin-bottom-10 post">
                <Typography
                  className="margin-top-10 margin-bottom-10"
                  color="textSecondary"
                  gutterBottom={true}
                >
                  <span
                    onClick={() => goToThread(postAndThread.thread.id as number)}
                    className="post-thread-text margin-bottom-10 cursor-pointer"
                  >
                    In thread {postAndThread.thread.title}
                  </span>
                </Typography>
                <CardContent>
                  <div>{postAndThread.post.content}</div>
                </CardContent>
              </Card>
            ))}
          </div>
        </div>
      </Card>
    </div>
  );
}

export { Profile as ProfileComponent };

const mapStateToProps = (state: User) => {
  return {
    user: state,
  };
};

export default connect(mapStateToProps, {})(Profile);
