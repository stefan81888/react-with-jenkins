import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { HomepageComponent } from './Homepage';
import { act } from 'react-dom/test-utils';
import { unmountComponentAtNode } from 'react-dom';
import { Subforum } from '../../model/subforum';
import { SubforumService } from '../../data-access/subforums';
import { TopicService } from '../../data-access/topic';
import { Topic } from '../../model/topic';
import { UsersService } from '../../data-access/users';
import { User } from '../../model/user';

const subforumsServiceMock: SubforumService = ({
  getSubforums: () => {
    return Promise.resolve([
      { id: 0, title: 'test', topics: [] },
      { id: 0, title: 'another-subforum', topics: [] },
    ]);
  },
  editSubforum: () => {
    return new Promise(() => {
      return {} as Subforum;
    });
  },
} as unknown) as SubforumService;
const usersServiceMock = ({
  getUsers: () => {
    return Promise.resolve([]);
  },
} as unknown) as UsersService;

const mockUser: User = {
  imageUrl: '',
  password: 'encrypted',
  username: 'stefan81888',
  isAdmin: true,
  isLoggedIn: true,
};

const topicServiceMock: TopicService = {
  addTopic: (topic: Topic) => {
    return new Promise(() => {
      return {} as Topic;
    });
  },
} as TopicService;

let container: any = null;
beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

describe('Homepage', () => {
  it('does not open dialog', async () => {
    await act(() => {
      render(
        <HomepageComponent
          usersService={usersServiceMock}
          user={mockUser}
          subforumService={subforumsServiceMock}
          topicService={topicServiceMock}
        />,
        container
      );
    });

    const dialog = document.querySelector('[data-testid=dialog]');
    expect(dialog).toBeFalsy();
  });

  it('opens dialog', async () => {
    await act(() => {
      render(
        <HomepageComponent
          usersService={usersServiceMock}
          user={mockUser}
          subforumService={subforumsServiceMock}
          topicService={topicServiceMock}
        />,
        container
      );
    });

    const newTopicButton = document.querySelector('[data-testid=new-topic]');

    act(() => {
      newTopicButton?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });

    const dialog = document.querySelector('[data-testid=dialog]');
    expect(dialog).toBeTruthy();
  });

  it('displays subforums', async () => {
    const spy = jest.spyOn(subforumsServiceMock, 'getSubforums');

    await act(async () => {
      render(
        <HomepageComponent
          usersService={usersServiceMock}
          user={mockUser}
          subforumService={subforumsServiceMock}
          topicService={topicServiceMock}
        />,
        container
      );
    });

    expect(spy).toHaveBeenCalledTimes(1);
    const firstSubforum = document.querySelector('[data-testid=test]');
    expect(firstSubforum).toBeTruthy();
    const secondSubforum = document.querySelector('[data-testid=another-subforum]');
    expect(secondSubforum).toBeTruthy();
  });

  it('adds topic', async () => {
    const spy = jest.spyOn(topicServiceMock, 'addTopic');

    await act(async () => {
      render(
        <HomepageComponent
          usersService={usersServiceMock}
          user={mockUser}
          subforumService={subforumsServiceMock}
          topicService={topicServiceMock}
        />,
        container
      );
    });

    const newTopicButton = document.querySelector('[data-testid=new-topic]');

    act(() => {
      newTopicButton?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });

    const input = document.querySelector('[data-testid=topic]');
    fireEvent.change(input as Element, { target: { value: 'New topic' } });
    expect((input as HTMLInputElement).value).toBe('New topic');

    const button = document.querySelector('[data-testid=add-topic]');

    act(() => {
      button?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith({
      title: 'New topic',
    } as Topic);
  });
});
