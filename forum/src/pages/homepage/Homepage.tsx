import React, { useState, useEffect } from 'react';
import './Homepage.css';
import Subforum from '../../ui-components/subforum/Subforum';
import { Subforum as SubforumModel } from '../../model/subforum';
import { SubforumService } from '../../data-access/subforums';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { TopicService } from '../../data-access/topic';
import { Topic } from '../../model/topic';
import Members from '../../ui-components/members/Members';
import { UsersService } from '../../data-access/users';
import { User } from '../../model/user';
import { connect } from 'react-redux';

function Homepage(props: {
  subforumService: SubforumService;
  topicService: TopicService;
  usersService: UsersService;
  user: User;
}) {
  const user = props.user;
  const subforumService = props.subforumService;
  const topicService = props.topicService;
  const usersService = props.usersService;
  const [subforums, setSubforums] = useState<SubforumModel[]>([]);
  const [users, setUsers] = useState<User[]>([]);
  const [isAddTopicDialogOpen, setIsAddTopicDialogOpen] = useState<boolean>(false);
  const [newTopicTitle, setNewTopicName] = useState<string>('');
  const [selectedSubforum, setSelectedSubforum] = useState<number>(1);

  useEffect(() => {
    subforumService.getSubforums().then((allSubforums: SubforumModel[]) => {
      setSubforums(allSubforums);
    });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    usersService.getUsers().then((allUsers: User[]) => {
      setUsers(allUsers);
    });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const onAddTopicDialogClose = (): void => {
    setIsAddTopicDialogOpen(false);
  };

  const onNewTopicTitleChange = (event: any): void => {
    setNewTopicName(event.target.value);
  };

  const onSubforumChange = (event: any): void => {
    setSelectedSubforum(event.target.value);
  };

  const addTopic = (): void => {
    topicService
      .addTopic({
        title: newTopicTitle,
      })
      .then((newTopic) => {
        const subforum = subforums.find((x) => x.id === selectedSubforum);

        if (subforum) {
          subforum.topics = [...subforum?.topics, newTopic];
          subforumService.editSubforum(subforum).then((modifiedSubforum) => {
            setNewTopicName('');
            setSelectedSubforum(1);
            setIsAddTopicDialogOpen(false);
          });
        }
      });
  };

  const removeTopic = (topic: Topic): void => {
    topicService.removeTopic(topic).then(() => {
      const subforumToBeEdited = subforums.find((x) =>
        x.topics.map((y) => y.id).includes(topic.id)
      );

      if (subforumToBeEdited) {
        subforumToBeEdited.topics = subforumToBeEdited?.topics.filter(
          (x) => x.id !== topic.id
        ) as Topic[];
        subforumService.editSubforum(subforumToBeEdited as SubforumModel).then((editedSubforum) => {
          subforumService.getSubforums().then((allSubforums: SubforumModel[]) => {
            setSubforums(allSubforums);
          });
        });
      }
    });
  };

  return (
    <div className="flex flex-column">
      <div className="margin-bottom-10">
        <h2 className="float-left">Subforums</h2>
        <div className="float-right add-topic">
          {user.isLoggedIn ? (
            <Button
              data-testid="new-topic"
              onClick={() => {
                setIsAddTopicDialogOpen(true);
              }}
              variant="contained"
              color="primary"
            >
              New topic
            </Button>
          ) : null}
        </div>
      </div>
      <div>
        {subforums.map((subforum, index) => (
          <div data-testid={subforum.title} className="full-width" key={index}>
            <Subforum
              user={user}
              subforum={subforum}
              removeTopic={(topic: Topic) => removeTopic(topic)}
            />
          </div>
        ))}
      </div>
      {user.isLoggedIn ? (
        <div data-testid="members" className="margin-top-10">
          <Members users={users} />
        </div>
      ) : null}
      <Dialog open={isAddTopicDialogOpen} onClose={onAddTopicDialogClose}>
        <div data-testid="dialog" className="add-topic-dialog">
          <form className="flex flex-column">
            <TextField
              inputProps={{ 'data-testid': 'topic' }}
              className="add-thread-input margin-top-10"
              value={newTopicTitle}
              onChange={onNewTopicTitleChange}
              label="Topic name"
            />
            <Select
              labelId="demo-simple-select-placeholder-label-label"
              id="demo-simple-select-placeholder-label"
              className="add-thread-input margin-top-20"
              value={selectedSubforum}
              onChange={onSubforumChange}
              autoWidth={true}
              data-testid={'select-subforum'}
            >
              {subforums.map((subforum, index) => (
                <MenuItem
                  data-testid={'subforum-' + subforum.title}
                  key={index}
                  value={subforum.id}
                >
                  {subforum.title}
                </MenuItem>
              ))}
            </Select>
            <Button
              data-testid="add-topic"
              disabled={!newTopicTitle}
              onClick={addTopic}
              className="add-topic-button margin-top-20 margin-bottom-10 float-right"
              variant="contained"
              color="primary"
            >
              Add topic
            </Button>
          </form>
        </div>
      </Dialog>
    </div>
  );
}

export { Homepage as HomepageComponent };

const mapStateToProps = (state: User) => {
  return {
    user: state,
  };
};

export default connect(mapStateToProps, {})(Homepage);
