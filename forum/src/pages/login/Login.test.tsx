import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { unmountComponentAtNode } from 'react-dom';
import { LoginComponent } from './Login';
import { UsersService } from '../../data-access/users';
import { User } from '../../model/user';

let usersMock: User[] = [];
const loginCallback = jest.fn();

const userServiceMock = ({
  getUsers: () => {
    return Promise.resolve(usersMock);
  },
} as unknown) as UsersService;

describe('Login page', () => {
  let container: any = null;
  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it('does not log user in', async () => {
    usersMock = [{ username: 'user', password: 'admin' } as User];

    await act(async () => {
      render(<LoginComponent login={loginCallback} usersService={userServiceMock} />, container);
    });

    const usernameInput = document.querySelector('[data-testid=login-username]');
    fireEvent.change(usernameInput as Element, { target: { value: 'admin' } });
    const passwordInput = document.querySelector('[data-testid=login-password]');
    fireEvent.change(passwordInput as Element, { target: { value: 'admin' } });
    const loginButton = document.querySelector('[data-testid=login-button]');

    expect(usernameInput).toBeTruthy();
    expect(passwordInput).toBeTruthy();
    expect(loginButton).toBeTruthy();

    act(() => {
      loginButton?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });

    expect(loginCallback).not.toHaveBeenCalled();
  });

  it('does not log user when password is incorrect', async () => {
    usersMock = [{ username: 'user', password: 'admin' } as User];

    await act(async () => {
      render(<LoginComponent login={loginCallback} usersService={userServiceMock} />, container);
    });

    const usernameInput = document.querySelector('[data-testid=login-username]');
    fireEvent.change(usernameInput as Element, { target: { value: 'admin' } });
    const passwordInput = document.querySelector('[data-testid=login-password]');
    fireEvent.change(passwordInput as Element, { target: { value: 'pass' } });
    const loginButton = document.querySelector('[data-testid=login-button]');

    expect(usernameInput).toBeTruthy();
    expect(passwordInput).toBeTruthy();
    expect(loginButton).toBeTruthy();

    act(() => {
      loginButton?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });

    expect(loginCallback).not.toHaveBeenCalled();
  });

  it('logs in existing user', async () => {
    usersMock = [{ username: 'admin', password: 'admin' } as User];

    await act(async () => {
      render(<LoginComponent login={loginCallback} usersService={userServiceMock} />, container);
    });

    const usernameInput = document.querySelector('[data-testid=login-username]');
    fireEvent.change(usernameInput as Element, { target: { value: 'admin' } });
    const passwordInput = document.querySelector('[data-testid=login-password]');
    fireEvent.change(passwordInput as Element, { target: { value: 'admin' } });
    const loginButton = document.querySelector('[data-testid=login-button]');

    expect(usernameInput).toBeTruthy();
    expect(passwordInput).toBeTruthy();
    expect(loginButton).toBeTruthy();

    act(() => {
      loginButton?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });

    expect(loginCallback).toHaveBeenCalled();
    expect(loginCallback).toHaveBeenLastCalledWith({ username: 'admin', password: 'admin' });
  });
});
