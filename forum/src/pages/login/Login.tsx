import React, { useState, useEffect } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import './Login.css';
import { User } from '../../model/user';
import { UsersService } from '../../data-access/users';
import { useHistory } from 'react-router-dom';
import NotificationModal from '../../ui-components/notification-modal/NotificationModal';
import { connect } from 'react-redux';
import { login } from '../../store/login/login.actions';

function Login(props: { usersService: UsersService; login: CallableFunction }) {
  const usersService = props.usersService;
  const history = useHistory();

  const [user, setUser] = useState<User>({
    username: '',
    password: '',
    imageUrl: '',
    isAdmin: false,
    isLoggedIn: false,
  });
  const [users, setUsers] = useState<User[]>([]);
  const [isNotificationModalOpen, setIsNotificationDialogOpen] = useState<boolean>(false);

  useEffect(() => {
    usersService.getUsers().then((allUsers: User[]) => {
      setUsers([...allUsers]);
    });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const onLoginFormInputsChange = (event: any): void => {
    const { name: propertyName, value } = event.target;
    setUser({ ...user, [propertyName]: value });
  };

  const logUserIn = (): void => {
    const userFromDb = users.find((u) => u.username === user.username);
    if (!userFromDb || userFromDb.password !== user.password) {
      setUser({
        username: '',
        password: '',
        imageUrl: '',
        isAdmin: false,
        isLoggedIn: false,
      });
      setIsNotificationDialogOpen(true);
    } else {
      props.login(userFromDb);
      history?.push('/home');
    }
  };

  const onNotificationModalClose = (): void => {
    setIsNotificationDialogOpen(false);
  };

  const isLoginFormDisabled = (): boolean => {
    return !user.username || !user.password;
  };

  return (
    <div className="flex flex-center">
      <form className="flex flex-column flex-center login-form">
        <TextField
          inputProps={{ 'data-testid': 'login-username' }}
          placeholder="Username"
          type="text"
          name="username"
          value={user.username}
          onChange={onLoginFormInputsChange}
        />
        <TextField
          inputProps={{ 'data-testid': 'login-password' }}
          className="margin-top-10"
          placeholder="Password"
          type="text"
          name="password"
          value={user.password}
          onChange={onLoginFormInputsChange}
        />
        <Button
          data-testid="login-button"
          disabled={isLoginFormDisabled()}
          className="margin-top-10 login-button"
          onClick={logUserIn}
          variant="contained"
          color="primary"
        >
          Login
        </Button>
      </form>
      <NotificationModal
        data-testid="modal"
        onNotificationModalClose={onNotificationModalClose}
        open={isNotificationModalOpen}
        message="Username or password are not correct"
      />
    </div>
  );
}

export { Login as LoginComponent };

export default connect(
  () => {
    return {};
  },
  { login }
)(Login);
