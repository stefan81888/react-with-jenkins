const { baseAppUrl } = require("./base-app-url")

describe('Thread page', () => {
    it('Adds post to thread and then removes it', () => {
        const postContent = Math.random().toString(36).substring(7);
        cy.visit(`${baseAppUrl}`);
        cy.login('admin', 'admin');
        cy.contains('Sports').click();
        cy.contains('Basketball').click();
        cy.contains('NBA').click();
        cy.addPost(postContent);
        cy.home();
        cy.contains('Sports').click();
        cy.contains('Basketball').click();
        cy.contains('NBA').click();
        cy.contains(`${postContent}`);
        cy.removePost(postContent);
    })
  })
