const { baseAppUrl } = require("./base-app-url")

describe('Admin page', () => {
    it('Adds and then removes subforum', () => {
        const subforumName = 'Test';
        cy.visit(`${baseAppUrl}`);
        cy.login('admin', 'admin');
        cy.goToAdminPage();
        cy.addSubforum(subforumName);
        cy.goToAdminPage();
        cy.removeSubforum(subforumName);
    })

    it('Adds and then removes admin rights to user', () => {
        cy.visit(`${baseAppUrl}`);
        cy.login('admin', 'admin');
        cy.goToAdminPage();
        cy.addAdminRightsToUser('tatum');
        cy.removeAdminRightsFromUser('tatum');
    })

    it('Creates and then removes admin user', () => {
        cy.visit(`${baseAppUrl}`);
        cy.login('admin', 'admin');
        cy.goToAdminPage();
        cy.createAdminUser('test-user', 'test-pass', 'test-image');
        cy.removeAdminRightsFromUser('test-user');
        cy.deleteUser('test-user');
    })
  })