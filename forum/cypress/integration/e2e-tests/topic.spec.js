const { baseAppUrl } = require("./base-app-url")

describe('Topic page', () => {
    it('Adds new thread', () => {
        cy.visit(`${baseAppUrl}`);
        cy.login('ratm', 'ratm');
        cy.contains('Sports').click();
        cy.contains('Basketball').click();
        cy.contains('New thread').click();
        cy.addThread('test thread', "test post");
        cy.logout();
        cy.login('admin', 'admin');
        cy.contains('Sports').click();
        cy.contains('Basketball').click();
        cy.removeThread('test thread');
    })
  })
