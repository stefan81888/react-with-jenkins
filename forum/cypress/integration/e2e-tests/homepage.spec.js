const { baseAppUrl } = require("./base-app-url")

describe('Homepage', () => {
    it('Displays homepage', () => {
        cy.visit(`${baseAppUrl}home`);
        cy.contains('Subforums');
        cy.contains('Sports');
        cy.contains('Politics');
        cy.contains('Film and series');
        cy.contains('Music');
        cy.contains('Culture');
    })

    it('Adds and then removes new topic', () => {
        cy.visit(`${baseAppUrl}`);
        cy.login('aly', 'aly');
        cy.addNewTopic('test-topic', 'Culture');
        cy.logout();
        cy.login('admin', 'admin');
        cy.removeTopic('test-topic', 'Culture');
    })
  })