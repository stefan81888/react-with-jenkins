const { baseAppUrl } = require("./base-app-url")

describe('Register user page', () => {
    it('Registers and then removes user', () => {
        cy.visit(`${baseAppUrl}register`);
        cy.register('test-user', 'test-pass', 'test-image');
        cy.goToProfilePageOfUser('test-user');
        cy.logout();
        cy.login('admin', 'admin');
        cy.goToAdminPage();
        cy.deleteUser('test-user');
    })
  })
