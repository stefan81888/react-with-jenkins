const { baseAppUrl } = require("./base-app-url")

describe('Edit user page', () => {
    it('Edits username of the user', () => {
        cy.visit(`${baseAppUrl}`);
        cy.login('ratm', 'ratm');
        cy.goToProfilePageOfUser('ratm');
        cy.goToEditProfilePage();
        cy.editUserUsername('ratm1');
        cy.goToEditProfilePage();
        cy.editUserUsername('ratm');
    })
  })
