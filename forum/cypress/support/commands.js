const { baseAppUrl } = require("../integration/e2e-tests/base-app-url");

Cypress.Commands.add('login', (username, password) => {
    cy.get('[data-testid=login-username]').type(username);
    cy.get('[data-testid=login-password]').type(password);
    cy.get('[data-testid=login-button]:first').click();
    cy.url().should('include', '/home');
    cy.get('[data-testid=members]');
    goToProfilePageOfUser(username);
    goToHomepage();
})

Cypress.Commands.add('logout', () => {
    cy.get('[data-testid=options]').click();
    cy.get('[data-testid=logout]').click();
})

Cypress.Commands.add('home', () => {
   goToHomepage();
})

function goToHomepage() {
    cy.get('[data-testid=home]').click();
    cy.url().should('include', '/home');
}

Cypress.Commands.add('addPost', (postContent) => {
    cy.get('[data-testid=post]').type(postContent);
    cy.get('[data-testid=add]').click();
})

Cypress.Commands.add('removePost', (postContent) => {
    cy.get(`[data-testid=post-delete-${postContent}]`).click();
    cy.get(`[data-testid=confirm]`).click();
    cy.reload();
    cy.get(`#${postContent}`).should('not.exist');
})

Cypress.Commands.add('register', (username, password, imageUrl) => {
   addUser(username, password, imageUrl, 'Register');
   cy.url().should('include', '/home');
   goToProfilePageOfUser(username);
})

Cypress.Commands.add('createAdminUser', (username, password, imageUrl) => {
    addUser(username, password, imageUrl, 'Add admin user')
 })

function addUser(username, password, imageUrl, buttonLabel) {
    cy.get('[data-testid=username]').type(username);
    cy.get('[data-testid=password]').type(password);
    cy.get('[data-testid=image]').type(imageUrl);
    cy.contains(buttonLabel).click();
}
 
Cypress.Commands.add('goToAdminPage', () => {
    cy.get('[data-testid=options]').click();
    cy.get('[data-testid=admin]').click();
    cy.url().should('include', '/admin');
})

Cypress.Commands.add('goToProfilePageOfUser', (username) => {
    goToProfilePageOfUser(username);
})

function goToProfilePageOfUser(username) {
    cy.get('[data-testid=options]').click();
    cy.get('[data-testid=profile]').click();
    cy.url().should('include', '/profile');
    cy.get('[data-testid=username]').should('have.text', username);
}

Cypress.Commands.add('deleteUser', (username) => {
    const selector = `#${username}`;
    cy.get('[data-testid="select-Delete user"]').click();
    cy.get(selector).should('exist');
    cy.get(selector).click();
    cy.get('[data-testid="button-Delete user"]').click();
    cy.visit(`${baseAppUrl}admin`);
    cy.get(selector).should('not.exist');
})

Cypress.Commands.add('goToEditProfilePage', () => {
    cy.url().should('include', '/profile');
    cy.get('[data-testid=edit-profile-button]').click();
    cy.url().should('include', '/edit-user');
})

Cypress.Commands.add('editUserUsername', (newUsername) => {
    cy.get('[data-testid=username]').clear().type(newUsername);
    cy.contains('Edit').click();
    cy.get('[data-testid=username]').should('have.text', newUsername);
})

Cypress.Commands.add('addSubforum', (subforumName) => {
    cy.get('[data-testid=subforum-name]').type(subforumName);
    cy.get('[data-testid=add-subforum]').click();
    cy.get('[data-testid=home]').click();
    cy.contains(subforumName);
    cy.get(`[data-testid=${subforumName}]`).should('exist');
})

Cypress.Commands.add('removeSubforum', (subforumName) => {
    cy.get(`[data-testid=remove-${subforumName}]`).click();
    cy.get('[data-testid=confirm]').click();
    cy.get('[data-testid=home]').click();
    cy.get(`[data-testid=${subforumName}]`).should('not.exist');
})

Cypress.Commands.add('addAdminRightsToUser', (username) => {
    const selector = `#${username}`;
    cy.get('[data-testid="select-Add admin"]').click();
    cy.get(selector).click();
    cy.get(selector).should('have.text', username);
    cy.get('[data-testid="button-Add admin"]').click();
    cy.visit(`${baseAppUrl}admin`);
})

Cypress.Commands.add('removeAdminRightsFromUser', (username) => {
    const selector = `#${username}`;
    cy.get('[data-testid="select-Remove admin"]').click();
    cy.get(selector).click();
    cy.get(selector).should('have.text', username);
    cy.get('[data-testid="button-Remove admin"]').click();
    cy.visit(`${baseAppUrl}admin`);
    cy.get('[data-testid="select-Add admin"]').click();
    cy.get(selector).should('have.text', username);
    cy.get(selector).click();
})

Cypress.Commands.add('addNewTopic', (topicName, subforumName) => {
    cy.get('[data-testid="new-topic"]').click();
    cy.get('[data-testid=topic]').type(topicName);
    const subforumSelector = `[data-testid="subforum-${subforumName}"]`
    cy.get(`[data-testid="select-subforum"]`).click();
    cy.get(subforumSelector).should('exist');
    cy.get(subforumSelector).click();
    cy.get('[data-testid="add-topic"]').click();
    cy.visit(`${baseAppUrl}home`);
    cy.get(`[data-testid=${subforumName}]`).click();
    cy.get(`[data-testid="topic-${topicName}"]`).should('exist');
})

Cypress.Commands.add('removeTopic', (topicName, subforumName) => {
    cy.get(`[data-testid="${subforumName}"]`).click();
    cy.get(`#${topicName}`).click();
    cy.visit(`${baseAppUrl}home`);
    cy.get(`[data-testid="${subforumName}"]`).click();
    cy.get(`#${topicName}`).should('not.exist');
})

Cypress.Commands.add('addThread', (threadName, initialPost) => {
    cy.get('[data-testid=new-topic-name]').type(threadName);
    cy.get('[data-testid=post]').type(initialPost);
    cy.get(`[data-testid=add-thread]`).click();
    cy.contains(threadName);
})

Cypress.Commands.add('removeThread', (threadName) => {
    cy.get(`[data-testid="delete-thread-${threadName}"]`).click();
    cy.reload();
    cy.get(`[data-testid="delete-thread-${threadName}"]`).should('not.exist');
})




